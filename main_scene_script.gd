extends Node

var url = "https://api.openai.com/v1/completions" # End point to send queries to ChatGPT
var url_models = "https://api.openai.com/v1/models" # End point to get a list of the current Models
var model = "davinci-002" # the chosen model for this example
var api_key : String = "your-api-key-here"

var headers : Array
var body : Dictionary
var query : String
var json_response : Dictionary

func _ready():
	build_headers()

func _on_button_pressed():
	request_API() 

func request_API():
	$HTTPRequest.request_completed.connect(_on_request_completed)
	$HTTPRequest.request(url, headers, HTTPClient.METHOD_POST, build_body())

func _on_request_completed(result, response_code, headers, body):
	json_response = JSON.parse_string(body.get_string_from_utf8())
	$TextEdit_Response.text = str(json_response)

func build_headers():
	if StaticData.secrets != null:
		api_key = StaticData.secrets["chat_gpt"]
	headers = ["Content-Type: application/json", "Authorization: Bearer " + api_key]

func build_body():
	query = $TextEdit_Query.text
	body = {"prompt": query, "model": model}
	return JSON.stringify(body)
