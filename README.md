# Godot ChatGPT API Integration

## Description

A simple example of how to integrate ChatGPT API into a Godot project. If you just want to see the script, check out [`main_scene_script.gd`](https://gitlab.com/MajoMirez/godot-chatgpt-api/-/blob/main/main_scene_script.gd) in the root folder

## Getting Started

### Dependencies

* This example works for Godot 4.2 (`v4.2.1.stable.official [b09f793f5]`)
* This example uses the model `davinci-002`
* This example requires you to provide your own API key (check out https://platform.openai.com/ for more information)

### How to Execute

* Clone repository
* In the repository folder, create a folder named `secrets`
* Inside the secrets folder, create a JSON file named `secrets.json`, this JSON must contain your ChatGPT API key with this structure:
	```
	{
	"chat_gpt": "YOUR-API-KEY-HERE"
	}
	```
* Open project with Godot 4.2 and play test it
* Alternatively, you can just hard-code your API key into the code for testing, but FOR THE LOVE OF GOD, NEVER EVER EXPOSE YOUR API KEYS!!

## Authors

María José Ramírez [@MajoMirez](https://gitlab.com/MajoMirez)
