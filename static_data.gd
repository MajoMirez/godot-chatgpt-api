extends Node

var secrets = {}
var data_file_path = "res://secrets/secrets.json"

func _ready():
	secrets = load_json_file(data_file_path)
	#print(secrets)

func load_json_file(filePath : String):
	if FileAccess.file_exists(filePath):
		var dataFile = FileAccess.open(filePath, FileAccess.READ)
		var parsedResult = JSON.parse_string(dataFile.get_as_text())
		if parsedResult is Dictionary:
			return parsedResult
		else:
			print("ERROR reading file")
	else:
		print("ERROR file doesnt exist")
